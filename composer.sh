#/bin/bash

set -e

# Create Deployment Key File
echo -e "$DEPLOYMENT_KEY" >> $HOME/.ssh/id_rsa
chmod 400 ~/.ssh/id_rsa

cd /app

composer install