# Composer for Codeship #

Supports adding an SSL cert from the `DEPLOYMENT_KEY` environment variable so that Codeship builds can include an encrypted deployment key to fetch dependencies.

## Usage ##

In your `codeship-services.yml`, map a volume containing your source code to `/app`, ensuring that the `composer.json` file is in the root directory of the volume.

```yaml
composer:
  image: webartisans/codeship-composer
  volumes:
    - ./dist:/app
```
In your `codeship-steps.yml` add the composer command you would like to run.

In this case, we're running `composer install`.

All arguments passed via the `command` attribute are passed through to composer within our `/composer.sh` entrypoint script.

```yaml
- name: composer_install
  service: composer
  command: install
```